package io.piveau.importing.sparql;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.piveau.importing.utils.SparqlConnector;
import io.piveau.pipe.connector.PipeContext;
import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.web.client.WebClient;

public class ImportingSparqlVerticle extends AbstractVerticle {

    public static final String ADDRESS = "io.piveau.pipe.importing.sparql.queue";

    private WebClient client;

    private CircuitBreaker breaker;

    @Override
    public void start(Future<Void> startFuture) {
        vertx.eventBus().consumer(ADDRESS, this::handlePipe);

        client = WebClient.create(vertx);

        breaker = CircuitBreaker.create("sparql-breaker", vertx, new CircuitBreakerOptions()
                .setMaxRetries(2)
                .setTimeout(30000))
                .retryPolicy(count -> count * 15000L);

        startFuture.complete();
    }

    private void handlePipe(Message<PipeContext> message) {
        PipeContext pipeContext = message.body();
        JsonNode config = pipeContext.getConfig();
        pipeContext.log().info("Import started.");

        String catalogueUriRef = config.path("catalogueUriRef").asText("");

        SparqlConnector connector = SparqlConnector.create(client, breaker, pipeContext);

        Future<Long> countFuture = Future.future();
        connector.getTotalCount(catalogueUriRef, countFuture);
        countFuture.compose(totalCount -> {
            Future<Void> finishFuture = Future.future();

            connector.init(totalCount);
            connector.fetchPage(catalogueUriRef, finishFuture);

            return finishFuture;
        }).setHandler(ar -> {
            if (ar.succeeded()) {
                pipeContext.log().info("Import metadata finished");
                vertx.setTimer(5000, t -> {
                    pipeContext.setResult(new JsonArray(connector.getIdentifiers()).encodePrettily(), "application/json", new ObjectMapper().createObjectNode().put("content", "identifierList")).forward(client);
                });
            } else {
                pipeContext.log().error("Import metadata finished", ar.cause());
            }
        });

    }

}
