package io.piveau.importing.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.piveau.pipe.connector.PipeContext;
import io.piveau.utils.DigestAuth;
import io.piveau.utils.Hash;
import io.piveau.utils.JenaUtils;
import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DCAT;

import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class SparqlConnector {

    private WebClient client;

    private String address;
    private String username;
    private String password;

    private CircuitBreaker breaker;

    private long offset = 0;
    private long totalCount = 0;
    private List<String> identifiers = new ArrayList<>();

    private String outputFormat;

    private PipeContext pipeContext;

    public static SparqlConnector create(WebClient client, CircuitBreaker breaker, PipeContext pipeContext) {
        return new SparqlConnector(client, breaker, pipeContext);
    }

    private SparqlConnector(WebClient client, CircuitBreaker breaker, PipeContext pipeContext) {
        this.client = client;
        this.breaker = breaker;
        this.pipeContext = pipeContext;

        JsonObject config = new JsonObject(pipeContext.getConfig().toString());
        this.username = config.getString("user");
        this.password = config.getString("password");
        this.address = config.getString("address");

        this.outputFormat = config.getString("outputFormat", "text/turtle");
    }

    public void getTotalCount(String catalogueUriRef, Handler<AsyncResult<Long>> handler) {
        String query;
        if (!catalogueUriRef.isEmpty()) {
            query = "SELECT (COUNT(?d) AS ?count) WHERE { GRAPH <" + catalogueUriRef + "> { ?s <" + DCAT.dataset + "> ?d } }";
        } else {
            query = "SELECT (COUNT(?s) AS ?count) WHERE { ?s a <" + DCAT.Dataset + "> }";
        }

        HttpRequest<Buffer> request = client.getAbs(address)
                .addQueryParam("default-graph-uri", "")
                .addQueryParam("query", query)
                .putHeader("Accept", "application/json");
        query(request, ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                ResultSet result = ResultSetFactory.fromJSON(new ByteArrayInputStream(response.bodyAsString().getBytes()));
                long totalCount = result.next().getLiteral("count").getLong();
                handler.handle(Future.succeededFuture(totalCount));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
    }

    public List<String> getIdentifiers() {
        return identifiers;
    }

    public void init(long totalCount) {
        offset = 0;
        identifiers.clear();
        this.totalCount = totalCount;
    }

    public void fetchPage(String catalogueUriRef, Future<Void> future) {
        getGraphList(catalogueUriRef, ar -> {
            if (ar.succeeded()) {
                List<String> graphNames = ar.result();
                if (graphNames.isEmpty()) {
                    future.complete();
                } else {
                    graphNames.forEach(graphName -> getDataset(graphName, gr -> {
                        if (gr.succeeded()) {
                            Model dataset = gr.result();
                            Resource metadata = dataset.getResource(graphName);
                            String identifier = JenaUtils.findIdentifier(metadata);
                            identifiers.add(identifier);
                            Model extracted = JenaUtils.extractResource(metadata);
                            String pretty = JenaUtils.write(extracted, outputFormat);
                            ObjectNode dataInfo = new ObjectMapper().createObjectNode()
                                    .put("total", totalCount)
                                    .put("counter", identifiers.size())
                                    .put("identifier", identifier)
                                    .put("hash", Hash.asHexString(pretty));

                            pipeContext.setResult(pretty, outputFormat, dataInfo).forward(client);
                            pipeContext.log().info("Data imported: {}", dataInfo);

                            dataset.close();
                        } else {
                            pipeContext.log().error("Data error", gr.cause());
                        }
                    }));
                    offset += 100;
                    if (offset < totalCount) {
                        fetchPage(catalogueUriRef, future);
                    } else {
                        future.complete();
                    }
                }
            } else {
                future.fail(ar.cause());
            }
        });
    }

    public void getGraphList(String catalogueUriRef, Handler<AsyncResult<List<String>>> handler) {
        String query;
        if (!catalogueUriRef.isEmpty()) {
            query = "SELECT ?graph WHERE { GRAPH <" + catalogueUriRef + "> { ?s <" + DCAT.dataset + "> ?graph } } OFFSET " + offset + " LIMIT 100";
        } else {
            query = "SELECT ?graph WHERE { { SELECT DISTINCT ?graph WHERE { graph ?graph { ?s a <" + DCAT.Dataset + "> } } ORDER BY ?graph } } OFFSET " + offset + " LIMIT 100";
        }

        HttpRequest<Buffer> request = client.getAbs(address)
                .addQueryParam("default-graph-uri", "")
                .addQueryParam("query", query)
                .putHeader("Accept", "application/json");
        query(request, ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                ResultSet result = ResultSetFactory.fromJSON(new ByteArrayInputStream(response.body().getBytes()));
                List<String> graphList = new ArrayList<>();
                while (result.hasNext()) {
                    graphList.add(result.next().getResource("graph").getURI());
                }
                handler.handle(Future.succeededFuture(graphList));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
    }

    public void getDataset(String graphName, Handler<AsyncResult<Model>> handler) {
        String query = "CONSTRUCT WHERE { GRAPH <" + graphName + "> { ?s ?p ?o } }";
        HttpRequest<Buffer> request = client.getAbs(address)
                .addQueryParam("default-graph-uri", "")
                .addQueryParam("query", query)
                .putHeader("Accept", "application/n-triples");
        query(request, ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                Model dataset = ModelFactory.createDefaultModel();
                dataset.read(new StringReader(response.bodyAsString()), null, "application/n-triples");
                handler.handle(Future.succeededFuture(dataset));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
    }

    private void send(HttpRequest<Buffer> request, HttpMethod method, Handler<AsyncResult<HttpResponse<Buffer>>> handler) {
        request.send(ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                if (response.statusCode() == 401) {
                    String authenticate = DigestAuth.authenticate(response.getHeader("WWW-Authenticate"), address, method.name(), username, password);
                    if (authenticate != null) {
                        request.putHeader("Authorization", authenticate);
                        send(request, method, handler);
                    } else {
                        handler.handle(Future.failedFuture("Could not authenticate"));
                    }
                } else if (response.statusCode() == 200 || response.statusCode() == 201 || response.statusCode() == 204) {
                    handler.handle(Future.succeededFuture(response));
                } else {
                    handler.handle(Future.failedFuture(response.statusMessage()));
                }
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
    }

    public void query(HttpRequest<Buffer> request, Handler<AsyncResult<HttpResponse<Buffer>>> handler) {
        breaker.<HttpResponse<Buffer>>execute(fut -> send(request, HttpMethod.GET, fut))
                .setHandler(ar -> {
                    if (ar.succeeded()) {
                        handler.handle(Future.succeededFuture(ar.result()));
                    } else {
                        handler.handle(Future.failedFuture(ar.cause()));
                    }
                });
    }

}
